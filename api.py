from flask import Flask, request, jsonify, make_response, g, render_template
from flask_cors import CORS
import BaseXClient
from Sparql import  sendSparqlQuery
import threading
import xml.etree.ElementTree as ET
import xml.dom.minidom as dom


app = Flask(__name__)
CORS(app)
session = BaseXClient.Session('localhost', 1984, 'admin', 'admin')

@app.route('/', methods=['GET'])
def main():
    #globvar = session.query("db:open(\"Holzschnitt\")")
    session.execute("open Holzschnitte1")
    merke = []

    for i in range(1,10):
    	url = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[position() =" + str(i) +" ]/resource/resourceLink/text()) return $x" ).execute()
    	actor = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[position() =" + str(i) +" ]/descriptiveMetadata/eventWrap/actor/name/text()) return $x" ).execute()
    	title = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[position() =" + str(i) +" ]/descriptiveMetadata/objectIdentification/titleSet/title/text()) return $x" ).execute()
    	eintrag = {"url" : url, "actor" : actor, "title" : title}
    	merke.append(eintrag)

    	
    #tree = ET.parse(globvar)
    #root = tree.root()
    #globvar = "ws"
    #session.query("db:open(\"Holzschnitt\")").execute()


    return render_template('index.html', objects = merke, searchBool = False, text = '')

@app.route('/search', methods=['POST', 'GET'])
def search():
	if request.method=='GET':
		merke = []
		for i in range(1,5):
			url = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[position() =" + str(i) +" ]/resource/resourceLink/text()) return $x" ).execute()
			actor = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[position() =" + str(i) +" ]/descriptiveMetadata/eventWrap/actor/name/text()) return $x" ).execute()
			title = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[position() =" + str(i) +" ]/descriptiveMetadata/objectIdentification/titleSet/title/text()) return $x" ).execute()
			eintrag = {"url" : url, "actor" : actor, "title" : title}
			merke.append(eintrag)

		return render_template('index.html', objects = merke, searchBool = False, text = '')

	text = request.form['holzschnitte']
	#suche nach Kuenstler:
	#query = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/actor[contains(text(), " + text + ")]]) return $x")
	merke = []
	urlQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/eventWrap/actor/name[contains(lower-case(text()),'" + text.lower() + "')]]/resource/resourceLink/text()) return $x")
	actorQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/eventWrap/actor/name[contains(lower-case(text()),'" + text.lower() + "')]]/descriptiveMetadata/eventWrap/actor/name/text()) return $x")
	titleQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/eventWrap/actor/name[contains(lower-case(text()),'" + text.lower() + "')]]/descriptiveMetadata/objectIdentification/titleSet/title/text()) return $x")
	urlList = []
	actorList = []
	titleList = []

	for a,url in urlQuery.iter():
		urlList.append(url)

	for b,actor in actorQuery.iter():
		actorList.append(actor)

	for c,title in titleQuery.iter():
		titleList.append(title)


	for i in range(0, len(urlList)):
		url = urlList[i]
		actor = actorList[i]
		title = titleList[i]
		eintrag = {"url" : url, "actor" : actor, "title" : title}
		merke.append(eintrag)

	urlQuery.close()
	actorQuery.close()
	titleQuery.close()
	
	#Suche nach Titel:
	urlQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(lower-case(text()),'" + text.lower() + "')]]/resource/resourceLink/text()) return $x")
	actorQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(lower-case(text()),'" + text.lower() + "')]]/descriptiveMetadata/eventWrap/actor/name/text()) return $x")
	titleQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(lower-case(text()),'" + text.lower() + "')]]/descriptiveMetadata/objectIdentification/titleSet/title/text()) return $x")
	urlList = []
	actorList = []
	titleList = []

	for a,url in urlQuery.iter():
		urlList.append(url)

	for b,actor in actorQuery.iter():
		actorList.append(actor)

	for c,title in titleQuery.iter():
		titleList.append(title)


	for i in range(0, len(urlList)):
		url = urlList[i]
		actor = actorList[i]
		title = titleList[i]
		eintrag = {"url" : url, "actor" : actor, "title" : title}
		merke.append(eintrag)

	urlQuery.close()
	actorQuery.close()
	titleQuery.close()

	return render_template('index.html', objects = merke, searchBool = True, text = text)

@app.route('/filter', methods=['GET'])
def filter():
	merke = []
	merkeLetter = []
	actorQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in distinct-values(/root/object/descriptiveMetadata/eventWrap/actor/name/text()) order by $x return $x")
	for a,actor in actorQuery.iter():
		anfang = actor[0]
		if (len(merkeLetter) == 0):
			merkeLetter.append({"name" : anfang})
		elif anfang != merkeLetter[len(merkeLetter)-1]["name"]:
			merkeLetter.append({"name" : anfang})
		merke.append({"anfang" : anfang, "name" : actor})
	#Filter für ungefähres Datum:
	merkeDate = []
	dateQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in distinct-values(/root/object/descriptiveMetadata/eventWrap/eventDate/datum/text()) order by $x return $x")
	for a,datum in dateQuery.iter():
		merkeDate.append({"datum" : datum})
	#Filter für Ort:
	merkeOrt = []
	ortQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in distinct-values(/root/object/descriptiveMetadata/eventWrap/eventPlace/text()) order by $x return $x")
	for a,ort in ortQuery.iter():
		merkeOrt.append({"ort" : ort})

	return render_template('filter.html', letters = merkeLetter, objects = merke, dates = merkeDate, orte = merkeOrt,  filter = True, text = '')

@app.route('/filter/actor/<path:kunstler>', methods=['GET'])
def filterActor(kunstler):
	text = kunstler
	merke = []
	urlQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/actor[contains(text(),'" + kunstler + "')]]/bildLink/text()) return $x")
	actorQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/actor[contains(text(),'" + kunstler + "')]]/descriptiveMetadata/actor/text()) return $x")
	titleQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/actor[contains(text(),'" + kunstler + "')]]/descriptiveMetadata/objectIdentificationWrap/title/text()) return $x")

	urlList = []
	actorList = []
	titleList = []

	for a,url in urlQuery.iter():
		urlList.append(url)

	for b,actor in actorQuery.iter():
		actorList.append(actor)

	for c,title in titleQuery.iter():
		titleList.append(title)


	for i in range(0, len(urlList)):
		url = urlList[i]
		actor = actorList[i]
		title = titleList[i]
		eintrag = {"url" : url, "actor" : actor, "title" : title}
		merke.append(eintrag)

	urlQuery.close()
	actorQuery.close()
	titleQuery.close()

	return render_template('index.html', objects = merke, searchBool = True, text = text)

@app.route('/filter/ort/<path:ort>', methods=['GET'])
def filterOrt(ort):
	text = ort
	merke = []
	urlQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/eventWrap/eventPlace[contains(text(),'" + ort + "')]]/resource/resourceLink/text()) return $x")
	actorQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/eventWrap/eventPlace[contains(text(),'" + ort + "')]]/descriptiveMetadata/eventWrap/actor/name/text()) return $x")
	titleQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/eventWrap/eventPlace[contains(text(),'" + ort + "')]]/descriptiveMetadata/objectIdentification/titleSet/title/text()) return $x")

	urlList = []
	actorList = []
	titleList = []

	for a,url in urlQuery.iter():
		urlList.append(url)

	for b,actor in actorQuery.iter():
		actorList.append(actor)

	for c,title in titleQuery.iter():
		titleList.append(title)


	for i in range(0, len(urlList)):
		url = urlList[i]
		actor = actorList[i]
		title = titleList[i]
		eintrag = {"url" : url, "actor" : actor, "title" : title}
		merke.append(eintrag)

	urlQuery.close()
	actorQuery.close()
	titleQuery.close()

	return render_template('index.html', objects = merke, searchBool = True, text = text)

@app.route('/filter/datum/<path:datum>', methods=['GET'])
def filterDatum(datum):
	text = datum
	merke = []
	urlQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/eventWrap/eventDate/datum[contains(text(),'" + datum + "')]]/resource/resourceLink/text()) return $x")
	actorQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/eventWrap/eventDate/datum[contains(text(),'" + datum + "')]]/descriptiveMetadata/eventWrap/actor/name/text()) return $x")
	titleQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/eventWrap/eventDate/datum[contains(text(),'" + datum + "')]]/descriptiveMetadata/objectIdentification/titleSet/title/text()) return $x")

	urlList = []
	actorList = []
	titleList = []

	for a,url in urlQuery.iter():
		urlList.append(url)

	for b,actor in actorQuery.iter():
		actorList.append(actor)

	for c,title in titleQuery.iter():
		titleList.append(title)


	for i in range(0, len(urlList)):
		url = urlList[i]
		actor = actorList[i]
		title = titleList[i]
		eintrag = {"url" : url, "actor" : actor, "title" : title}
		merke.append(eintrag)

	urlQuery.close()
	actorQuery.close()
	titleQuery.close()

	return render_template('index.html', objects = merke, searchBool = True, text = text)

@app.route('/detail/<path:bild>', methods=['GET'])
def detail(bild):
	text = bild
	urlQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(text(),'" + bild + "')]]/resource/resourceLink/text()) return $x").execute()
	actorQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(text(),'" + bild + "')]]/descriptiveMetadata/eventWrap/actor/name/text()) return $x").execute()
	birth = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(text(),'" + bild + "')]]/descriptiveMetadata/eventWrap/actor/geburtsjahr/text()) return $x").execute()
	death = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(text(),'" + bild + "')]]/descriptiveMetadata/eventWrap/actor/todesjahr/text()) return $x").execute()
	place = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(text(),'" + bild + "')]]/descriptiveMetadata/eventWrap/eventPlace/text()) return $x").execute()
	mase = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(text(),'" + bild + "')]]/descriptiveMetadata/objectIdentification/displayObjectMeasurements/text()) return $x").execute()
	materialsTech = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(text(),'" + bild + "')]]/descriptiveMetadata/eventWrap/materialsTech/text()) return $x")
	materials = ""
	for a,material in materialsTech.iter():
		materials = materials + str(material) + ", " 

	tagQuery = session.query("declare namespace lido=\"http://www.lido-schema.org\"; for $x in (/root/object[descriptiveMetadata/objectIdentification/titleSet/title[contains(text(),'" + bild + "')]]/descriptiveMetadata/subjectWrap/subject/term/text()) return $x")
	tags = ""
	for t,tag in tagQuery.iter():
		tags = tags + str(tag) + ", " 
	kunstler = sendSparqlQuery(actorQuery)
	kunstlerDict = {"name" : kunstler[0],"geschlecht" : kunstler[1], "geburtsdatum" : kunstler[2],"todesdatum" : kunstler[3],"beschreibung" : kunstler[4]}
	merke = {"url" : urlQuery, "actor" : actorQuery, "birth" : birth, "death" : death, "title" : bild, "place" : place, "maße" : mase, "materials" : materials, "tags" : tags}
	return render_template('detail.html', object = merke, kunstler = kunstlerDict)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)