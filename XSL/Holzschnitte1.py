#!/usr/bin/python
# -*- coding: UTF-8 -*-
from lxml import builder
from lxml import etree

xml_input = etree.parse("./mkg_lido--mkgddb-sub-hack-profil_20170310 .xml")
xslt_root = etree.parse("./Holzschnitte1.xsl")
transform = etree.XSLT(xslt_root)
newdom = transform(xml_input)
f = file('./Holzschnitte1.xml', 'w')

#f.write(buffer)
print >>f, etree.tostring(newdom, pretty_print=True)
f.close()
