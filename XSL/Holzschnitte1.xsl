<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lido="http://www.lido-schema.org">

	<xsl:strip-space element="root" />

	<xsl:output method="xml"/>
<!-- Root -->
	<xsl:template match="/">
		<root>
			<xsl:apply-templates/>
		</root>
	</xsl:template>

<!-- einzelne Objekte -->
	<xsl:template match="lido:lido">
		<xsl:choose>
			<xsl:when test="./lido:descriptiveMetadata/lido:objectClassificationWrap/lido:objectWorkTypeWrap/lido:objectWorkType/lido:term[contains(@lido:pref, 'preferred')] = 'Holzschnitt'">
					<object>
						<xsl:apply-templates/>
					</object>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

<!-- ID -->
	<xsl:template match="lido:lidoRecID">
		<recID>
			<xsl:value-of select="."/>
		</recID>
	</xsl:template>

	<xsl:template match="lido:conceptID">
	</xsl:template>

<!-- Metadaten -->
	<xsl:template match="lido:descriptiveMetadata">
		<descriptiveMetadata>
			<xsl:apply-templates/>
		</descriptiveMetadata>
	</xsl:template>

<!-- Objektdaten Material -->
	<xsl:template match="lido:objectClassificationWrap">
		<objectClassification>
			<xsl:apply-templates/>
		</objectClassification>
	</xsl:template>

	<xsl:template match="lido:objectWorkTypeWrap">
		<objectWorkType>
			<xsl:attribute name="type">
				<xsl:value-of select="./lido:objectWorkType[@lido:type]"/>
			</xsl:attribute>
			<xsl:value-of select="./lido:objectWorkType/lido:term"/>
		</objectWorkType>
	</xsl:template>

	<xsl:template match="lido:classificationWrap">
		<classification>
			<xsl:attribute name="type">
				<xsl:value-of select="./lido:classification[@lido:type]"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="./lido:classification/lido:term[@lido:pref = 'preferred']">
					<xsl:value-of select="./lido:classification/lido:term"/>
				</xsl:when>
			</xsl:choose> 
		</classification>
	</xsl:template>

<!-- Objektdaten Inhalt -->
	<xsl:template match="lido:objectIdentificationWrap">
		<objectIdentification>
			<xsl:apply-templates/>
		</objectIdentification>
	</xsl:template>

	<xsl:template match="lido:titleWrap">
		<titleSet>
			<xsl:attribute name="type">
				<xsl:value-of select="./lido:titleSet[@lido:type]"/>
			</xsl:attribute>
			<title>
				<xsl:value-of select="./lido:titleSet/lido:appellationValue"/>
			</title>
		</titleSet>
	</xsl:template>

	<xsl:template match="lido:inscriptionsWrap"/>

	<xsl:template match="lido:repositoryWrap"/>

	<xsl:template match="lido:objectDescriptionWrap"/>

	<xsl:template match="lido:objectMeasurementsWrap/lido:objectMeasurementsSet/lido:displayObjectMeasurements">
		<displayObjectMeasurements>
			<xsl:value-of select="."/>
		</displayObjectMeasurements>
	</xsl:template>

<!-- event -->
	<xsl:template match="lido:eventWrap">
		<eventWrap>
			<xsl:apply-templates/>
		</eventWrap>
	</xsl:template>
	
	<xsl:template match="lido:eventType">
		<type>
			<xsl:value-of select="./lido:term"/>
		</type>
	</xsl:template>
<!-- Künstler -->
	<xsl:template match="lido:eventActor">
		<actor>
			<xsl:attribute name="type">
				<xsl:value-of select="./lido:actorInRole/lido:actor[@lido:type]"/>
			</xsl:attribute>
			<name>
				<xsl:value-of select="./lido:actorInRole/lido:actor/lido:nameActorSet/lido:appellationValue"/>
			</name>
			<geburtsjahr>
				<xsl:value-of select="./lido:actorInRole/lido:actor/lido:vitalDatesActor/lido:earliestDate"/>
			</geburtsjahr>
			<todesjahr>
				<xsl:value-of select="./lido:actorInRole/lido:actor/lido:vitalDatesActor/lido:latestDate"/>
			</todesjahr>
			<roleActor>
				<xsl:value-of select="./lido:actorInRole/lido:roleActor/lido:term"/>
			</roleActor>
		</actor>
	</xsl:template>
<!-- Datum -->
	<xsl:template match="lido:eventDate">
		<eventDate>
			<datum>
				<xsl:value-of select="lido:displayDate"/>
			</datum>
			<earliest>
				<xsl:value-of select="lido:date/lido:earliestDate"/>
			</earliest>
			<latest>
				<xsl:value-of select="lido:date/lido:latestDate"/>
			</latest>
		</eventDate>
	</xsl:template>
<!-- Ort -->
	<xsl:template match="lido:eventPlace">
		<eventPlace>
			<xsl:value-of select="lido:displayPlace"/>
		</eventPlace>
	</xsl:template>	

	<xsl:template match="lido:eventMaterialsTech">
		<materialsTech>
			<xsl:attribute name="type">
				<xsl:value-of select="./lido:materialsTech/lido:termMaterialsTech[@lido:type]"/>
			</xsl:attribute>
			<xsl:value-of select="./lido:materialsTech/lido:termMaterialsTech/lido:term"/>
		</materialsTech>
	</xsl:template>

	<xsl:template match="lido:objectRelationWrap/lido:subjectWrap">
		<subjectWrap>
			<subject>
				<xsl:attribute name="type">
					<xsl:value-of select="./subjectSet/lido:subject[@lido:type]"/>
				</xsl:attribute>
				<term>
					<xsl:value-of select="./subjectSet/lido:subject/lido:subjectConcept/lido:term"/>
				</term>
			</subject>
		</subjectWrap>
	</xsl:template>

	<xsl:template match="lido:administrativeMetadata">
		<recordLink>
			<xsl:value-of select="lido:recordWrap/lido:recordInfoSet/lido:recordInfoLink"/>
		</recordLink>
		<resource>
			<resourceLink>
				<xsl:value-of select="lido:resourceWrap/lido:resourceSet/lido:resourceRepresentation/lido:linkResource"/>
			</resourceLink>
		</resource>
	</xsl:template>	

</xsl:stylesheet>

