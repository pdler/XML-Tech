from SPARQLWrapper import SPARQLWrapper, JSON
from flask import jsonify
sparql = SPARQLWrapper("http://dbpedia.org/sparql")


#Bekommt den Namen des Kuenstlers
#Gibt einen Tupel (Name, Geschlecht, Geburtsdatum, Todesdatum, Beschreibung) zurueck
def sendSparqlQuery(name):
    if "(" in name:
        name = name.split(" (")[0]
    print("Name :" + name+": ")
    results = send(name)
    print("Empfange")
    test = results['results']['bindings']
    print(len(test))
    if(len(test) >= 1):
        tests = test
        for test in tests:
            name = test['name']['value']
            gender = test['gender']['value']
            birth = test['birth']['value']
            death = test['death']['value']
            description = test['description']['value']
            return (name, gender, birth, death, description)
    else:
        return ("No Information", "No Information", "No Information", "No Information", "No Information")





def send(name):
    print("Bearbeite")
    print("""
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        PREFIX dbo: <http://dbpedia.org/ontology/>#


        select ?person ?name ?birth ?description where {
          ?person a dbo:Person ;
                  foaf:name ?name ;
                  dbo:birthDate ?birth ;
                  dbo:abstract ?description       
          filter langMatches(lang(?description),'en')

          FILTER regex(str(?name), """+ "\"" + name+ "\"" +""")
        }
        order by ?person
        """)
    sparql.setQuery("""
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        PREFIX dc: <http://purl.org/dc/elements/1.1/>
        PREFIX : <http://dbpedia.org/resource/>
        PREFIX dbpedia2: <http://dbpedia.org/property/>
        PREFIX dbpedia: <http://dbpedia.org/>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX dbo: <http://dbpedia.org/ontology/>#


        select ?person ?name ?gender ?birth ?death ?description where {
          ?person a dbo:Person ;
                  foaf:name ?name ;
                  foaf:gender ?gender;
                  dbo:birthDate ?birth ;
                  dbo:deathDate ?death;
                  dbo:abstract ?description       
          filter langMatches(lang(?description),'en')

          FILTER regex(str(?name), """+ "\"" + name+ "\"" +""")
        }
        order by ?person
        """)
    sparql.setReturnFormat(JSON)
    return  sparql.query().convert()


