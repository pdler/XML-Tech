from flask import Flask, request, jsonify, make_response, g, render_template
from flask_cors import CORS
import BaseXClient
import threading
import xml.etree.ElementTree as ET
import xml.dom.minidom as dom


app = Flask(__name__)
CORS(app)
session = BaseXClient.Session('localhost', 1984, 'admin', 'admin')


session.execute("open Holzschnitt")


url = session.query("for $actor in distinct-values(//root/object/bildLink/text()) return $actor" ).execute()
links = url.split("\n")
file_object  = open("saveImages.sh", "w")

for link in links:
    file_object.write("wget " + link + "\n")

    print("wget " + link)

print(len(links))